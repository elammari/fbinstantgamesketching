﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantFB : MonoBehaviour {
	public delegate void OnTopScoresReceivedDelegate(int[]Scores,string[]UserNames);
	public OnTopScoresReceivedDelegate OnTopScoresReceived;

	public delegate void OnOwnScoreReceivedDelegate(int Score);
	public OnOwnScoreReceivedDelegate OnOwnScoreReceived;

	public delegate void OnUserNameReceivedDelegate(string Name);
	public OnUserNameReceivedDelegate OnUserNameReceived;

	public delegate void OnRewardedAdLoadedDelegate();
	public OnRewardedAdLoadedDelegate OnRewardedAdLoaded;

	public delegate void OnNormalAdLoadedDelegate();
	public OnNormalAdLoadedDelegate OnNormalAdLoaded;

	public delegate void OnNormalAdShownDelegate();
	public OnNormalAdShownDelegate OnNormalAdShown;

	public delegate void OnRewardedAdShownDelegate();
	public OnRewardedAdShownDelegate OnRewardedAdShown;


	public void SaveScore(int score,string LeaderBoardName)
	{
		Application.ExternalCall("AddNewScore",LeaderBoardName,score);
	}

	public void RequestOwnTopScore(string LeaderBoardName)
	{
		Application.ExternalCall ("RequestOwnScore",LeaderBoardName);
	}
	public void RequestTopScores(string BoardName,int count=10)
	{
		Application.ExternalCall ("RequestTopScores", BoardName, count);
	}
	public void RequestUserName()
	{
		Application.ExternalCall ("RequestUser");
	}


	public void RequestInt(string AdID)
	{
		Application.ExternalCall ("ReqInt",AdID);

	}
	public void ShowInt(string AdID)
	{
		Application.ExternalCall ("ShowInt",AdID);

	}
	public void RequestRwd(string AdID)
	{
		Application.ExternalCall ("ReqRwd",AdID);

	}
	public void ShowRwd(string AdID)
	{
		Application.ExternalCall ("ShowRwd",AdID);

	}


	public void RWDLoaded()
	{
		OnRewardedAdLoaded ();
	}
	public void INTLoaded()
	{
		OnNormalAdLoaded ();
	}


	public void RWDshown()
	{
		OnRewardedAdShown ();
	}
	public void INTshown()
	{
		OnNormalAdShown ();	
	}

	public void ReceiveUserName(string name)
	{
		OnUserNameReceived (name);
	}
	public void ReceiveOwnScore(int score)
	{
		OnOwnScoreReceived (score);
	}
	public void ReceiveScore(string scoreList)
	{
		int j = 0;
		string[] NewScoreList = scoreList.Split('|');
		int[] IntScoreList=new int[NewScoreList.Length/2];
		string[] UserNames=new string[NewScoreList.Length/2];
		for (int i = 0; i < IntScoreList.Length; i++) {
			IntScoreList [j] = int.Parse (NewScoreList [i]);
			i++;
			UserNames [j] = NewScoreList [i];
			j++;
		}
		OnTopScoresReceived (IntScoreList,UserNames);
	}


	// Use this for initialization
	void Start () {
		gameObject.name="FBInstantObject";
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
