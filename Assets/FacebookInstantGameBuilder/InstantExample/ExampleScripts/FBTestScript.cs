﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FBTestScript : MonoBehaviour {
	public InstantFB FBScript;
	public InputField BoardName, Score,BoardCount,INTID,RWDID;
	public Button showINT,showRWD;
	public Text Logs;

	void Update()
	{

	}
	// Use this for initialization
	void Start () {

		showRWD.interactable = false;
		showINT.interactable = false;
		FBScript.OnTopScoresReceived += OnScoresReceived;
		FBScript.OnOwnScoreReceived += OnOwnScoreReceived;
		FBScript.OnNormalAdLoaded += onIntLoaded;
		FBScript.OnRewardedAdLoaded += onRwdLoaded;
	}

	public void ClearConsole()
	{
		Logs.text = "";
	}
	public void LogConsole(string log)
	{
		Logs.text = Logs.text + "\n" + log;
	}


	public void SaveScore()
	{
		FBScript.SaveScore (int.Parse(Score.text), BoardName.text);
		LogConsole ("ScoreSaved");
	}
	public void LoadBoard()
	{
		FBScript.RequestTopScores (BoardName.text,int.Parse(BoardCount.text));
	}
	public void OnScoresReceived (int[] scores,string[] names)
	{
		LogConsole ("ScoreReceived");
		for(int i=0;i<scores.Length;i++)
			LogConsole(scores[i].ToString()+" "+names[0]);
	}


	public void LoadOwn()
	{
		FBScript.RequestOwnTopScore (BoardName.text);	
	}
	public void OnOwnScoreReceived(int scr)
	{
		LogConsole ("own top score:" + scr.ToString ());
	}





	public void LoadInt()
	{
		FBScript.RequestInt (INTID.text);
	}
	public void onIntLoaded()
	{
		showINT.interactable = true;
		LogConsole ("normal ad loaded");

	}

	public void ShowInt()
	{
		FBScript.ShowInt(INTID.text);
		showINT.interactable = false;

	}




	public void LoadRWD()
	{
		FBScript.RequestRwd(RWDID.text);
	}
	public void onRwdLoaded()
	{
		showRWD.interactable = true;
		LogConsole ("rewarded video loaded");
	}
	public void ShowRWD()
	{
		FBScript.ShowRwd (RWDID.text);
		showRWD.interactable = false;

	}


}
