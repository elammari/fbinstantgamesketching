using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FreeDraw
{
    // Helper methods used to set drawing settings
    public class DrawingSettings : MonoBehaviour
    {
        public static bool isCursorOverUI = false;
        public float Transparency = 1f;
        public Image WidthSliderIcon;
        public Image TransparancySliderIcon;


        public Color blue;
        public Color green;
        public Color red;
        public Color gray;



        // Changing pen settings is easy as changing the static properties Drawable.Pen_Colour and Drawable.Pen_Width
        public void SetMarkerColour(Color new_color)
        {
            Drawable.Pen_Colour = new_color;
        }
        // new_width is radius in pixels
        public void SetMarkerWidth(int new_width)
        {
            Drawable.Pen_Width = new_width;
        }
        public void SetMarkerWidth(float new_width)
        {
            SetMarkerWidth((int)new_width);

            WidthSliderIcon.rectTransform.localScale = new Vector2(new_width/5, new_width/5);


        }

        public void SetTransparency(float amount)
        {
            Transparency = amount;
            Color c = Drawable.Pen_Colour;
            c.a = amount;
             Drawable.Pen_Colour = c;
            
          TransparancySliderIcon.color = new Color(TransparancySliderIcon.color.r, TransparancySliderIcon.color.g, TransparancySliderIcon.color.b, amount);

        }


        // Call these these to change the pen settings
        public void SetMarkerGray()
        {
            Color c = gray;
            WidthSliderIcon.color = gray;
            TransparancySliderIcon.color = gray;

            c.a = Transparency;

            SetMarkerColour(c);
            Drawable.drawable.SetPenBrush();

        }
        public void SetMarkerRed()
        {
            Color c = red;
            WidthSliderIcon.color = red;
            TransparancySliderIcon.color = red;
            c.a = Transparency;
            SetMarkerColour(c);
            Drawable.drawable.SetPenBrush();
        }
        public void SetMarkerGreen()
        {
            Color c = green;
            WidthSliderIcon.color = green;
            TransparancySliderIcon.color = green;
            c.a = Transparency;
            SetMarkerColour(c);
            Drawable.drawable.SetPenBrush();
        }
        public void SetMarkerBlue()
        {
            Color c = blue;
            WidthSliderIcon.color = blue;
            TransparancySliderIcon.color = blue;
            c.a = Transparency;
            SetMarkerColour(c);
            Drawable.drawable.SetPenBrush();
        }
        public void SetEraser()
        {
            SetMarkerColour(new Color(255f, 255f, 255f, 0f));
        }

        public void PartialSetEraser()
        {
            SetMarkerColour(new Color(255f, 255f, 255f, 0.5f));
        }
    }
}